# FeeCalc
Multi-purpose Fee Calculator

# Requirements
GTK+ 3.20

# Install
There is no installation needed.

# Run
Just launch the python script: 
<br>
$python FeeCalc.py

# Donations
1LSPNZF9v5jKswGoRmPaS2HCNggX197U2W
