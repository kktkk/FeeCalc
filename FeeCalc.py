#!/usr/bin/env python

import sys
from decimal import Decimal as d, getcontext

getcontext().prec = 12

try:
    import gi

    gi.require_version("Gtk", "3.0")
    from gi.repository import Gtk
    from gi.repository import GObject as gobject
except:
    print "GTK3.0 not found, please verify your system."
    pass
    try:
        from gtk import gtk as Gtk
        import gobject
    except:
        print "GTK not found, please verify your system."
        sys.exit(1)


class Base:

    def __init__(self):
        # Set the Glade file
        self.builder = Gtk.Builder()  # create an instance of the gtk.Builder
        self.builder.add_from_file("FeeCalc.glade")  # add the xml file to the Builder
        self.window = self.builder.get_object("MainWindow")  # This gets the 'window' object
        self.about = self.builder.get_object("AboutWindow")
        self.fee = self.builder.get_object("EntryFee")
        self.profit = self.builder.get_object("EntryProfit")
        self.result = self.builder.get_object("Result")
        self.builder.connect_signals(self)
        self.window.show()

    # Main window handler
    def on_MainWindow_destroy(self, object, data=None):
        print "Window quit."
        Gtk.main_quit()

    # About event handler
    def on_AboutButton_clicked(self, *args):
        print "About Selected"
        self.response = self.about.run()
        self.about.hide()

    # Calculator function
    def calculate(self, *args):
        print "Calculating."
        fee = d(self.fee.get_text())
        profit = d(self.profit.get_text())
        result = profit - (profit * fee / 100)
        self.result.set_text(str(result))


if __name__ == "__main__":
    main = Base()  # create an instance of our class
    Gtk.main()  # run the thing
